# copyutils

High-level utilities to copy files and backup experiment data/metadata based on shutils.

CLI usage:

Copy multiple experiment to backup by name:

backup-experiment "{180113_JC049_2P_JC,180114_JC049_2P_JC}"

backup-experiment "180113_{JC049,JC060}_2P_JC"

Copy from backup:

get-experiment <experiment name>