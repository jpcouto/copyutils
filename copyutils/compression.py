import time
import sys
import os
import numpy as np
from glob import glob
from os.path import join as pjoin

from tifffile import imread, TiffFile
from tifffile import TiffWriter as twriter
from skvideo.io import FFmpegWriter

def sbx_get_info(sbxfile):
    ''' 
    Read info from a scanbox mat file [pass the sbx extension].
    info = sbx_get_info(sbxfile)

    Joao Couto
    '''
    matfile = os.path.splitext(sbxfile)[0] + '.mat'
    if not os.path.exists(matfile):
        raise('Metadata not found: {0}'.format(matfile))
    from scipy.io import loadmat
    info = loadmat(matfile,squeeze_me=True,struct_as_record=False)
    return info['info']

def sbx_get_shape(sbxfile):
    ''' 
    Get shape from scanbox file.
    Reads it from the file size and the info mat file.
    (chan,ncols,nrows,max_idx),nplanes = sbx_get_shape(sbxfile)

    Joao Couto 
    '''
    info = sbx_get_info(sbxfile)
    fsize = os.path.getsize(sbxfile)
    nrows,ncols = info.sz
    chan = info.channels
    if chan == 1:
        chan = 2; 
    elif chan == 2:
        chan = 1
    elif chan == 3:
        chan = 1
    max_idx = os.path.getsize(sbxfile)/nrows/ncols/chan/2
    if max_idx != info.config.frames:
        raise(Warning('sbx filesize doesnt match mat [{0},{1}]'.format(
                    max_idx,
                    info.config.frames)))
    nplanes = 1
    if not isinstance(info.otwave,int):
        if len(info.otwave) and info.volscan:
            nplanes = len(info.otwave)
    return (int(chan),int(ncols),int(nrows),int(max_idx)),nplanes


def mmap_scanbox(filename):
    '''
    Memory maps a scanbox file.

    npmap = sbx_memmap(filename,reshape_planes=True)
    Returns a N x 1 x NChannels x H x W memory map object; data can be accessed like a numpy array.
    Reshapes data to (N,nplanes,nchan,H,W) if plane_axis=1

    Actual data are 65535 - sbxmmap; data format is uint16

    Joao Couto
    '''

    if filename[-3:] == 'sbx':
        sbxshape,nplanes = sbx_get_shape(filename)
    return np.memmap(filename,
                     dtype='uint16',
                     shape=sbxshape,order='F').transpose([3,0,2,1]).reshape(
                         int(sbxshape[3]/nplanes),
                         nplanes,
                         sbxshape[0],
                         sbxshape[2],
                         sbxshape[1])


def mmap_dat(filename,
             mode = 'r',
             nframes = None,
             shape = None,
             dtype='uint16'):
    '''
    Loads frames from a binary file as a memory map.
    This is useful when the data does not fit to memory.
    
    Inputs:
        filename (str)       : fileformat convention, file ends in _NCHANNELS_H_W_DTYPE.dat
        mode (str)           : memory map access mode (default 'r')
                'r'   | Open existing file for reading only.
                'r+'  | Open existing file for reading and writing.                 
        nframes (int)        : number of frames to read (default is None: the entire file)
        shape (list|tuple)   : dimensions (NCHANNELS, HEIGHT, WIDTH) default is None
        dtype (str)          : datatype (default uint16) 
    Returns:
        A memory mapped  array with size (NFRAMES,[NCHANNELS,] HEIGHT, WIDTH).

    Example:
        dat = mmap_dat(filename)

    Joao Couto - from wfield
    '''
    
    if not os.path.isfile(filename):
        raise OSError('File {0} not found.'.format(filename))
    if shape is None or dtype is None: # try to get it from the filename
        meta = os.path.splitext(filename)[0].split('_')
        if shape is None:
            try: # Check if there are multiple channels
                shape = [int(m) for m in meta[-4:-1]]
            except ValueError:
                shape = [int(m) for m in meta[-3:-1]]
        if dtype is None:
            dtype = meta[-1]
    dt = np.dtype(dtype)
    if nframes is None:
        # Get the number of samples from the file size
        nframes = int(os.path.getsize(filename)/(np.prod(shape)*dt.itemsize))
    dt = np.dtype(dtype)
    return np.memmap(filename,
                     mode=mode,
                     dtype=dt,
                     shape = (int(nframes),*shape))


def stack_to_mj2_lossless(stack,fname, rate = 30):
    '''
    Compresses a uint16 stack with FFMPEG and libopenjpeg
    
    Inputs:
        stack                : array or memorymapped binary file
        fname                : output filename (will change extension to .mov)
        rate                 : rate of the mj2 movie [30 Hz default]

    Example:
       from labcams.io import * 
       fname = '20200710_140729_2_540_640_uint16.dat'
       stack = mmap_dat(fname)
       stack_to_mj2_lossless(stack,fname, rate = 30)

    Joao Couto - from labcams
    '''
    ext = os.path.splitext(fname)[1]
    assert len(ext), "[mj2 conversion] Need to pass a filename {0}.".format(fname)
    
    if not ext == '.mov':
        print('[mj2 conversion] Changing extension to .mov')
        outfname = fname.replace(ext,'.mov')
    else:
        outfname = fname
    assert stack.dtype == np.uint16, "[mj2 conversion] This only works for uint16 for now."

    nstack = stack.reshape([-1,*stack.shape[2:]]) # flatten if needed    
    sq = FFmpegWriter(outfname, inputdict={'-pix_fmt':'gray16le',
                                              '-r':str(rate)}, # this is important
                      outputdict={'-c:v':'libopenjpeg',
                                  '-pix_fmt':'gray16le',
                                  '-r':str(rate)})
    from tqdm import tqdm
    for i,f in tqdm(enumerate(nstack),total=len(nstack)):
        sq.writeFrame(65535-f)
    sq.close()
    
def tiffs_to_h264(tifffiles,
                  outputfolder,
                  frame_rate=30,
                  hwaccel = 'nvidia',
                  compression = 17):
    if hwaccel is None:                                                                                                                                                  
        doutputs = {'-format':'h264',                                                                                                                               
                    '-pix_fmt':'gray',                                                                                                                              
                    '-vcodec':'libx264',                                                                                                                            
                    '-threads':str(10),                                                                                                                             
                    '-crf':str(compression)}                                                                                                                   
    else:                                                                                                                                                                
        if hwaccel == 'intel':                                                                                                                                           
                doutputs = {'-format':'h264',                                                                                                                           
                            '-pix_fmt':'yuv420p',                                                                                                               
                            '-vcodec':'h264_qsv',                                                                                                            
                            '-global_quality':str(25), # specific to the qsv                                                                                            
                            '-look_ahead':str(1),                                                                                                                       
                            '-threads':str(1),                                                                                                                          
                            '-crf':str(compression)}                                                                                                               
        elif hwaccel == 'nvidia':                                                                                                                                        
                doutputs = {'-vcodec':'h264_nvenc',                                                                                                                     
                            '-pix_fmt':'yuv420p',                                                                                                                       
                            '-cq:v':'19',                                                                                                                               
                            '-preset':'fast'}                                                                                                                           
        doutputs['-r'] =str(frame_rate)
        dinputs = {'-r':str(frame_rate)}
    if not os.path.isdir(outputfolder):
        print('Creating output folder')
        os.makedirs(outputfolder)
        
    for f in tqdm(tifffiles):
        fname = pjoin(outputfolder,os.path.basename(f))
        fname = os.path.splitext(fname)[0]+'.avi'
        fd = FFmpegWriter(fname,                                                                                                                                     
                          inputdict=dinputs,                                                                                                                       
                          outputdict=doutputs)
        stack = imread(f)
        for s in stack:
            fd.writeFrame(s)
        fd.close()
