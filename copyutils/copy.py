from os.path import join as pjoin
from os import stat as fstat
from shutil import _samefile,stat,copystat

from .path import *
from .prefs import preferences

import shutil
import time
import os

from tqdm import tqdm
import numpy as np

import time
from multiprocessing import Pool
from functools import partial
import subprocess as sub

bufferlen = 16

def copyfile(src, dst, usesystem = True,
             length=bufferlen*1024,
             overwrite = False, statsonly = False, pbar = None):
    '''
    Adapted from shutils to include a progress bar.

    Joao Couto December 2018
    '''
    if _samefile(src, dst):
        raise OSError("{!r} and {!r} are the same file".format(src, dst))

    for fn in [src, dst]:
        try:
            st = os.stat(fn)
        except OSError:
            # File most likely does not exist
            pass
        else:
            # XXX What about other special files? (sockets, devices...)
            if stat.S_ISFIFO(st.st_mode):
                raise OSError("`%s` is a named pipe" % fn)
    if os.path.isfile(dst):
        if not overwrite and not statsonly:
            raise OSError("Can't copy when file already exists [{0}]".format(dst))
        elif statsonly:
            copystat(src, dst)
            return time.time()-tstart, counter
        else:
            print('Overwriting {0}'.format(dst))
    if os.path.islink(src):
        raise OSError('Skipping link: ' + src)
        return None,None
        #os.symlink(os.readlink(src), dst)
    else:
        counter = 0
        tstart = time.time()
        if not usesystem:
            with open(src, 'rb') as fsrc:
                if not os.path.isdir(os.path.dirname(dst)):
                    try:
                        os.makedirs(os.path.dirname(dst))
                    except FileExistsError:
                        pass
                    print('Created {0}.'.format(os.path.dirname(dst)))
                with open(dst, 'wb') as fdst:
                    while True:
                        buf = fsrc.read(length)
                        if not buf:
                            break
                        fdst.write(buf)
                        counter += len(buf)
                        if not pbar is None:
                            pbar.update(len(buf)/1e6)
                copystat(src, dst)
        else:
            if not os.path.isdir(os.path.dirname(dst)):
                try:
                    os.makedirs(os.path.dirname(dst))
                except FileExistsError:
                    pass
                print('Created {0}.'.format(os.path.dirname(dst)))
            sub.call('cp -r {0} {1}'.format(src,dst), shell=True)
            copystat(src, dst)
        return time.time()-tstart, counter

def copyfile_wrapper(srcdst, length=bufferlen*1024, overwrite = False, statsonly = False, pbar = None):
    return copyfile(srcdst[0],
                    srcdst[1],
                    length = length,
                    overwrite = overwrite,
                    statsonly = statsonly,
                    pbar = pbar)
    
########################################################################
##############SPECIFIC TO THE LAB DATA STRUCTURES#######################
########################################################################

def copy_experiment(exp_name,destfolder,expfolders = [],
                    datapaths = preferences['paths']['disks'],
                    nthreads = 8,
                    overwrite = False,
                    dryrun=True):
    ''' Copy experiment folders to a place.
    Example:
    tt = copy_experiment('181210_JC085_2P_JC','/quadraraid/data',['facecam'],preferences['paths'])

    Joao Couto January 2019

    '''
    tocopy = dict(dtype = [], srcdirs = [], dstdirs = [], srcfiles = [],dstfiles = [],nbytes = [])
    statsonly = False # This is for use in case stats differ and not in overwrite mode
    for itype,dtype in enumerate(expfolders):
        # search for each experiment type
        filesFound = 0
        selsrc = [] # source filename
        seldst = [] # destination filename
        selsrcdirs = []
        seldstdirs = []
        nbytes = 0
        for iserver in range(len(datapaths)):
            dserver = datapaths[iserver]
            if dserver == destfolder:
                print('Skipping {0}'.format(destfolder))
                continue
            dirs,files = recursivelist(pjoin(dserver,dtype,exp_name))
            if len(files):
                filesFound += len(files)
                print('\t -> Found dataset in {0} for {1} in {2}'.format(dtype,exp_name,dserver))
                files = [f for f in np.sort(files)]
                dstfiles = [f.replace(dserver,destfolder) for f in np.sort(files)]

                selsrc = [] # source filename
                seldst = [] # destination filename
                
                nbytes = 0
                for src,dst in zip(files,dstfiles):
                    srcstat = fstat(src)
                    if os.path.isfile(dst):
                        dststat = fstat(dst)
                        if not srcstat.st_size == dststat.st_size:
                            if not overwrite:
                                raise OSError('[FILESIZE] Check the copy of {1} in {0}.'.format(dst,src))
                            else:
                                print('[FILESIZE MISMATCH] Overwriting copy of {1} in {0}.'.format(dst,src))
                                selsrc.append(src)
                                seldst.append(dst)                 
                                nbytes += srcstat.st_size
                        elif not np.isclose(srcstat.st_mtime,dststat.st_mtime,rtol=0, atol=1e-03):
                            if not overwrite:
                                print('[STATS MISMATCH] trying to fix by copying stats from {1} to {0}.'.format(dst,src))
                                print('                 {0} != {1}'.format(srcstat.st_mtime,dststat.st_mtime))
                                statsonly = True
                                selsrc.append(src)
                                seldst.append(dst)                 
                                #raise OSError('[FILE STATS] Check the stats of {1} in {0}.'.format(dst,src))
                            else:
                                print('[STATS MISMATCH] Overwriting copy of {1} in {0}.'.format(dst,src))
                                selsrc.append(src)
                                seldst.append(dst)                 
                                nbytes += srcstat.st_size                            
                    else:
                        selsrc.append(src)
                        seldst.append(dst)
                        nbytes += srcstat.st_size
                # Do the dir stats
                srcdirs = dirs
                srcdirs.append(pjoin(dserver,dtype,exp_name))
                dstdirs = [f.replace(dserver,destfolder) for f in np.sort(srcdirs)]
                selsrcdirs = []
                seldstdirs = []
                for src,dst in zip(srcdirs,dstdirs):
                    srcstat = fstat(src)
                    if os.path.isdir(dst):
                        dststat = fstat(dst)
                        if not np.isclose(srcstat.st_mtime,dststat.st_mtime,rtol=0, atol=1e-03):
                            selsrcdirs.append(src)
                            seldstdirs.append(dst)
                    else:
                        selsrcdirs.append(src)
                        seldstdirs.append(dst)
                break  # Because we found files, moving on...
            else:
                print('\t [Warning] \t Could not find dataset {0} for {1} in {2}'.format(dtype,exp_name,dserver))
        if len(selsrc):
            print('\t - Marking {0} dataset for copy {1} in {2} [{3}]'.format(dtype,exp_name,dserver,nbytes))
            tocopy['srcfiles'].extend(selsrc)
            tocopy['dstfiles'].extend(seldst)
            tocopy['dtype'].append(dtype)
            tocopy['nbytes'].append(nbytes)
        if len(selsrcdirs):
            tocopy['srcdirs'].extend(selsrcdirs)
            tocopy['dstdirs'].extend(seldstdirs)            
        if not filesFound:
            print('\t [Warning] -> Could not find dataset {0} for {1} in anywhere'.format(dtype,exp_name))
    # Copy the files that are needed.
    if not dryrun:
        if len(tocopy['srcfiles']):
            print('Have {0} files to copy.'.format(len(tocopy['srcfiles'])))
            if nthreads>1:
                N = len(tocopy['dstfiles'])
                pool = Pool(nthreads)
                rs = pool.map_async(partial(copyfile_wrapper,
                                            overwrite = overwrite,
                                            statsonly = statsonly),
                                    zip(tocopy['srcfiles'],
                                        tocopy['dstfiles']))
                pool.close()
                with tqdm(desc = 'Copying files',
                          total=N) as pbar:
                    done = 0
                    while True:
                        if rs.ready():
                            break
                        if not done == N - rs._number_left:
                            done = N - rs._number_left
                            pbar.n = done
                            pbar.refresh()
                        time.sleep(0.25)
                pool.join()
            else:
                pbar = tqdm()
                totaltime = 0
                totalnbytes = 0
                for src,dst in zip(tocopy['srcfiles'],tocopy['dstfiles']):
                    pbar.set_description('{0}'.format(os.path.basename(src)))
                    tictoc,nbytes = copyfile(src,dst,
                                             pbar = pbar,
                                             overwrite = overwrite,
                                             statsonly = statsonly)
                    totaltime += tictoc
                    totalnbytes += nbytes
        else:
            print('Nothing to copy [{0}].'.format(exp_name))
        # Try to fix the folder permissions
        if len(tocopy['srcdirs']):
            for src,dst in tqdm(zip(tocopy['srcdirs'],tocopy['dstdirs']),desc = 'Setting folder stats'):
                if not os.path.isdir(dst):
                    print('WARNING -> Could not find folder [{0}]'.format(dst))
                else:
                    copystat(src, dst)
                    print('setting stats {0}'.format(dst))
    return tocopy


def experiment_to_backup(exp_name,datapaths = preferences['paths'],
                         backupserver = preferences['backup']['server'],
                         backupfolders = preferences['backup']['folders'],
                         overwrite = False,
                         dryrun = False,
                         nthreads = 4):
    ''' Backup data to server. Searches multiple experiments. Does not overwrite unless commanded.
    Example:
        tmp = experiment_to_backup('180912_JC078_2P_JC',nservers = 7)

    Joao Couto January 2019
    '''
    backupfolderpaths = []
    for dtype in backupfolders:
        if ':' in dtype:
            tmp = dtype.split(':')
            dtype = datapaths['{0}'.format(tmp[0])][tmp[1]]
        else:
            dtype = datapaths['{0}'.format(dtype)]
        backupfolderpaths.append(dtype)        
    cpdict = copy_experiment(exp_name,
                             destfolder = backupserver,
                             expfolders = backupfolderpaths,
                             datapaths = datapaths['disks'],
                             nthreads = nthreads,
                             overwrite = overwrite,
                             dryrun=dryrun)
    return cpdict


def tobackup():
    import sys
    from argparse import ArgumentParser
    parser = ArgumentParser(description='Backup experiments to remote server [Check the parameter file in ~/.copyutils]')
    parser.add_argument('expname',metavar='expname',
                        type=str,help='Name of the experiment. Can be for example "170810_{JC027,JC048}_2P_JC" to copy multiple experiments.')
    parser.add_argument('--overwrite',
                        action='store_true',
                        default=False,
                        help='Overwrites data if server does not match.')
    parser.add_argument('-n','--ncpus',
                        action='store',
                        default=8,
                        type=int,
                        help='Number of processes to use for copying simultaneously.')
    parser.add_argument('--dryrun',
                        action='store_true',
                        default=False,
                        help='Does a dry-run [Does not copy data].')

    opts = parser.parse_args()

    if opts.expname:
        import re
        enames = []
        # Check if there are numbers in brackets
        inbrackets = re.findall(r'\{(.*?)\}',opts.expname)
        nbrackets = len(inbrackets)
        if nbrackets:
            pairs = [s.split(',') for s in inbrackets]
            expname = str(opts.expname)
            for i in range(len(inbrackets)):
                expname = expname.replace(inbrackets[i],str(i))
            for zipped in zip(*pairs):
                enames.append(expname.format(*zipped))
        else:
            enames.append(opts.expname)
        for ename in enames:
            print('\n\t\t Backing up experiment: {0}\n'.format(ename))
            cpdict = experiment_to_backup(ename,
                                          overwrite=opts.overwrite,
                                          nthreads = opts.ncpus,
                                          dryrun = opts.dryrun)
            if opts.dryrun:
                print(cpdict)

#################################################################################
################################SCRIPTS##########################################
#################################################################################

def frombackup():
    import sys
    from argparse import ArgumentParser
    parser = ArgumentParser(description='Get an experiment from the backup server [Check the parameter file in ~/.copyutils]')
    parser.add_argument('expname',metavar='expname',
                        type=str,help='Name of the experiment. Can be for example "170810_{JC027,JC048}_2P_JC" to copy multiple experiments.')
    parser.add_argument('-f','--folders',
                        action='store',
                        default=preferences['backup']['folders'],
                        type=str,
                        nargs='+',
                        help='Folders to copy [eyecam presentation].')
    parser.add_argument('-s','--dest-server',
                        action='store',
                        default=preferences['paths']['disks'][0],
                        type=str,
                        help='Disk location path.')
    parser.add_argument('--overwrite',
                        action='store_true',
                        default=False,
                        help='Overwrites data if server does not match.')
    parser.add_argument('--dryrun',
                        action='store_true',
                        default=False,
                        help='Does a dry-run [Does not copy data].')
    parser.add_argument('-n','--ncpus',
                        action='store',
                        default=8,
                        type=int,
                        help='Number of processes to use for copying simultaneously.')

    opts = parser.parse_args()
    folderpaths = []
    datapaths = preferences['paths']
    for dtype in opts.folders:
        if ':' in dtype:
            tmp = dtype.split(':')
            dtype = datapaths['{0}'.format(tmp[0])][tmp[1]]
        else:
            dtype = datapaths['{0}'.format(dtype)]
        folderpaths.append(dtype)
    print(folderpaths)
    if opts.expname:
        import re
        enames = []
        # Check if there are numbers in brackets
        inbrackets = re.findall(r'\{(.*?)\}',opts.expname)
        nbrackets = len(inbrackets)
        if nbrackets:
            pairs = [s.split(',') for s in inbrackets]
            expname = str(opts.expname)
            for i in range(len(inbrackets)):
                expname = expname.replace(inbrackets[i],str(i))
            for zipped in zip(*pairs):
                enames.append(expname.format(*zipped))
        else:
            enames.append(opts.expname)
        for ename in enames:
            print('\n\t\t Experiment: {0}\n'.format(ename))
            cpdict = copy_experiment(ename,
                                     destfolder = opts.dest_server,
                                     expfolders = folderpaths,
                                     datapaths = [preferences['backup']['server']], 
                                     nthreads = opts.ncpus,
                                     overwrite = opts.overwrite,
                                     dryrun=opts.dryrun)
            if opts.dryrun:
                print(cpdict)
