import os
from os.path import join as pjoin

def dirstat(directory):
    '''
    dirstat - folder size and size of all files
    
    '''
    total = 0
    files = []
    try:
        for entry in os.scandir(directory):
            if entry.is_file():
                filesize = entry.stat().st_size
                total += filesize
                files.append((entry.path,filesize))
            elif entry.is_dir():
                t,f = dirstat(entry.path)
                total += t
                files.extend(f)
    except NotADirectoryError:
        sz = os.path.getsize(directory)
        return sz,[(directory,sz)]
    except PermissionError:
        return None,None
    return total,files


def recursivelist(path):
    ''' 
Recursive list files in path.
    
    dirs,files = recursivelist(path)

Joao Couto - December 2018
    '''
    
    dirs = []
    files = []
    for root, directories, filenames in os.walk(path):
        for directory in directories:
            dirs.append(pjoin(root, directory))
        for filename in filenames: 
            files.append(pjoin(root,filename))
    return dirs,files

import os, fnmatch


def find_files(directory, pattern):
    '''
Recursive find files that match a pattern.

Usage:
    for filename in find_files('/quadraraid/data/presentation/', '*JC027*.log'):
        print(filename)

Joao Couto January 2019
    '''
    for root, dirs, files in os.walk(directory):
        for basename in files:
            filename = os.path.join(root, basename)
            if fnmatch.fnmatch(filename, pattern):
                yield filename


