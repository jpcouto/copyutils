from glob import glob
import os
import sys
import json
from os.path import join as pjoin
from os.path import dirname, isdir, isfile

preferencepath = pjoin(os.path.expanduser('~'), '.copyutils')

defaultPreferences = {'paths':dict(disks = ['/mnt/humanserver/temp_data',
                                            '/quadraraid/data',
                                            '/scratch/data',
                                            '/mnt/bkrunch/j',
                                            '/mnt/bkrunch/h',
                                            '/mnt/bkrunch/g',
                                            '/mnt/bkrunch2/bdata/data',
                                            '/mnt/nerfhf01/boninwip/data',
                                            '/mnt/nerffs01/mouselab/data'],
                                   onephoton = '1photon/raw/{exp}',
                                   twophoton = dict(raw='2photon/raw/{exp}',
                                                    reg='2photon/reg/{exp}',
                                                    regds='2photon/reg_ds/{exp}',
                                                    projections = '2photon/reg_ds/{exp}'),
                                   ephys = dict(raw='ephys/raw/{exp}',
                                                sort='ephys/sort/{exp}'),
                                   log = 'presentation/{exp}',
                                   facecam = 'facecam/{exp}',
                                   eyecam = 'eyecam/{exp}'),
                                   #analysis = 'analysis/{exp}'),
                      'backup_compression':{
                          'twophoton:raw':'scanbox_compression',
                          'facecam':'ffmpeg_yuv',
                          'eyecam':'ffmpeg_yuv'},
                      'backup':dict(server = '/mnt/nerfhf01/boninwip/data',
                                    folders = ['log',
                                               'onephoton:raw',
                                               'facecam',
                                               'eyecam',
                                               'twophoton:raw',
                                               'twophoton:reg'])}
def get_preferences(preffile = preferencepath):
    ''' 
    Reads the user parameters from the home directory.
    
    pref = get_preferences(preference_filename)

    User parameters like folder location, file preferences, paths...

    Joao Couto - Feb 2018
    '''
    if not isdir(dirname(preffile)):
        os.makedirs(dirname(preffile))
        print('[copyutils] Creating preferences folder ['+preferencepath+']')


    if not isfile(preffile):
        with open(preffile, 'w') as outfile:
            json.dump(defaultPreferences, outfile, sort_keys = True, indent = 4)
            print('[copyutils] Saving default preferences to: ' + preffile)
    with open(preffile, 'r') as infile:
        pref = json.load(infile)
    return pref

preferences = get_preferences()
