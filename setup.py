#!/usr/bin/env python
# Install script for copy utils
# Joao Couto - Jan 2019

from setuptools import setup
from setuptools.command.install import install


longdescription = '''High-level utilities to copy files and backup experiment data/metadata implemented on top of shutils.'''
setup(
    name = 'copyutils',
    version = '0.0',
    author = 'Joao Couto',
    author_email = 'jpcouto@gmail.com',
    description = (longdescription),
    long_description = longdescription,
    license = 'GPL',
    packages = ['copyutils'],
    entry_points = {
        'console_scripts': [
            'backup-experiment = copyutils.copy:tobackup',
            'get-experiment = copyutils.copy:frombackup',
        ]
    },
)
